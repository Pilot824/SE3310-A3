#include <iostream>
#include <string>
#include <cmath>

using namespace std;

// Literally have to write a function to do this because the C++ compiler on linux has issues.
int char_to_int(char c){
	if (c == '0') return 0;
	if (c == '1') return 1;
	if (c == '2') return 2;
	if (c == '3') return 3;
	if (c == '4') return 4;
	if (c == '5') return 5;
	if (c == '6') return 6;
	if (c == '7') return 7;
	if (c == '8') return 8;
	if (c == '9') return 9;
}

int str_to_int(std::string s){

	int return_val = 0;

	for( int i = 0; i < s.length(); i++){
		return_val += char_to_int(s[i]) * pow(10.0, (s.length() - i - 1));
	}

	return return_val;
}