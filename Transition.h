#ifndef TRANSITION_H
#define TRANSITION_H

#include <iostream>
#include <string>

enum Direction {LEFT, RIGHT};

class Transition {
	int 		m_current_state;
	int 		m_next_state;
	char 		m_input_char;
	char 		m_output_char;
	Direction 	m_direction;

	public:
		// Default Constructor
		Transition();

		Transition( std::string current_state, std::string input_char, std::string next_state, std::string output_char, std::string direction);

		std::string display();
		void display_long();

		int current_state();
		int next_state();
		char input_char();
		char output_char();

		Direction direction();
};

#endif // TRANSITION_H