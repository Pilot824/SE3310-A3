#ifndef TMTAPE_H
#define TMTAPE_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Tape {

	vector<char> m_tape;
	int m_tape_pos;

	public:
		// Default Constructor
		Tape();

		void loadTape(string line);

		void display();

		char readTape();

		void moveHead(int amt);

		void test();

		void write(char c);
};

#endif // TMTAPE