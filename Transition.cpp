#include "Transition.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include "myFuncs.h"

using namespace std;

// Patch to allow the use of to_string in g++ compilation
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

Transition::Transition(){

	m_current_state 	= 0;
	m_next_state 		= 0;
	m_direction 		= LEFT;
	m_input_char		= '0';
	m_output_char		= '0';

}

// ALl char based input
Transition::Transition( std::string current_state, std::string input_char, std::string next_state, std::string output_char, std::string direction){

	m_current_state 	= str_to_int(current_state);
	m_next_state 		= str_to_int(next_state);


	if( input_char.length() > 1 || output_char.length() > 1 ){
		cout << "input_char length: " << input_char.length() << endl;
		cout << "output_char (" << output_char << ") length: " << output_char.length() << endl;
		throw 3;
	}

	m_input_char		= input_char[0];
	m_output_char		= output_char[0];

	if( direction == "L"){
		m_direction = LEFT;
	} else if( direction == "R"){
		m_direction = RIGHT;
	}

}

string Transition::display(){

	string result;

	result.append("t ");
	result.append(patch::to_string(m_current_state));
	result.append(" ");
	result.append(patch::to_string(m_input_char));
	result.append(" ");
	result.append(patch::to_string(m_next_state));
	result.append(" ");
	result.append(patch::to_string(m_output_char));
	result.append(" ");
	if( m_direction == LEFT ) result.append("L");
	else if( m_direction == RIGHT ) result.append("R");
	result.append(" ");

	return result;

}

void Transition::display_long(){

	cout << "Transition" 		<< endl;
	cout << "Current State: " 	<< m_current_state << endl;
	cout << "Input Character: "	<< m_input_char << endl;
	cout << "Next State: " 		<< m_next_state << endl;
	cout << "Output Char: " 	<< m_output_char << endl;
	cout << "Direction: ";

	if( m_direction == LEFT ) 		cout << "L";
	else if( m_direction == RIGHT ) cout << "R";

	cout << endl;

}

int Transition::current_state(){
	return m_current_state;
}

int Transition::next_state(){
	return m_next_state;
}

char Transition::input_char(){
	return m_input_char;
}

char Transition::output_char(){
	return m_output_char;
}

Direction Transition::direction(){
	return m_direction;
}